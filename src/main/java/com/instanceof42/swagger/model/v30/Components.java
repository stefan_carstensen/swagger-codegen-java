package com.instanceof42.swagger.model.v30;

import lombok.Data;

import java.util.Map;

/**
 * Holds a set of reusable objects for different aspects of the OAS.
 * All objects defined within the components object will have no effect on the API unless they are explicitly
 * referenced from properties outside the components object.
 * <p>
 * All the fixed fields declared are objects that MUST use keys that match the regular expression: ^[a-zA-Z0-9\.\-_]+$.
 * <p>
 * TODO This object MAY be extended with Specification Extensions.
 */
@Data
public class Components {

    /**
     * An object to hold reusable Schema Objects.
     */
    private Map<String, Schema> schemas;

    /**
     * An object to hold reusable Response Objects.
     */
    private Map<String, Response> responses;

    /**
     * An object to hold reusable Parameter Objects.
     */
    private Map<String, Parameter> parameters;

    /**
     * An object to hold reusable Example Objects.
     */
    private Map<String, Example> examples;

    /**
     * An object to hold reusable Request Body Objects.
     */
    private Map<String, RequestBody> requestBodies;

    /**
     * An object to hold reusable Header Objects.
     */
    private Map<String, Header> headers;


    /**
     * An object to hold reusable Security Scheme Objects.
     */
    private Map<String, SecurityScheme> securitySchemes;

    /**
     * An object to hold reusable Link Objects.
     */
    private Map<String, Link> links;

    /**
     * An object to hold reusable Callback Objects.
     */
    private Map<String, Callback> callbacks;

}
