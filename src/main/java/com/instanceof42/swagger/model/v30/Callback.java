package com.instanceof42.swagger.model.v30;

import lombok.Data;

import java.util.Map;

/**
 * A map of possible out-of band callbacks related to the parent operation.
 * Each value in the map is a Path Item Object that describes a set of requests that may be initiated by the API provider and the expected responses.
 * The key value used to identify the callback object is an expression, evaluated at runtime, that identifies a URL to use for the callback operation.
 * <p>
 * TODO This object MAY be extended with Specification Extensions.
 * <p>
 * TODO A specific parser is needed für the expressions
 */
@Data
public class Callback extends Reference {

    /**
     * A Path Item Object used to define a callback request and expected responses. A complete example is available.
     */
    private Map<String, PathItem> expression;

}
