package com.instanceof42.swagger.model.v30;

import lombok.Data;

@Data
public class Style {

    /**
     * Path-style parameters defined by RFC6570
     * primitive, array, object
     */
    private String matrix;

    /**
     * Label style parameters defined by RFC6570
     * primitive, array, object
     */
    private String label;

    /**
     * Form style parameters defined by RFC6570. This option replaces collectionFormat with a csv (when explode is false)
     * or multi (when explode is true) value from OpenAPI 2.0.
     * primitive, array, object
     */
    private String form;

    /**
     * Simple style parameters defined by RFC6570. This option replaces collectionFormat with a csv value from OpenAPI 2.0.
     * array
     */
    private String simple;

    /**
     * Space separated array values. This option replaces collectionFormat equal to ssv from OpenAPI 2.0.
     * array
     */
    private String spaceDelimited;

    /**
     * Pipe separated array values. This option replaces collectionFormat equal to pipes from OpenAPI 2.0.
     * array
     */
    private String pipeDelimited;


    /**
     * Provides a simple way of rendering nested objects using form parameters.
     * object
     */
    private String deepObject;

}
