package com.instanceof42.swagger.model.v30;

import lombok.Data;

/**
 * Contact information for the exposed API.
 * <p>
 * TODO This object MAY be extended with Specification Extensions.
 */
@Data
public class Contact {

    /**
     * The identifying name of the contact person/organization.
     */
    private String name;

    /**
     * The URL pointing to the contact information. MUST be in the format of a URL.
     */
    private String url;

    /**
     * The email address of the contact person/organization. MUST be in the format of an email address.
     */
    private String email;

}
