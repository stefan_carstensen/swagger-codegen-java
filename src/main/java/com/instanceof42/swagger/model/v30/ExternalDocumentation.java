package com.instanceof42.swagger.model.v30;

import lombok.Data;

/**
 * Allows referencing an external resource for extended documentation.
 * <p>
 * TODO This object MAY be extended with Specification Extensions.
 */
@Data
public class ExternalDocumentation {

    /**
     * A short description of the target documentation. CommonMark syntax MAY be used for rich text representation.
     */
    private String description;

    /**
     * REQUIRED. The URL for the target documentation. Value MUST be in the format of a URL.
     */
    private String url;

}
