package com.instanceof42.swagger.model.v30;

import lombok.Data;

import java.util.Map;

/**
 * Each Media Type Object provides schema and examples for the media type identified by its key.
 * <p>
 * TODO This object MAY be extended with Specification Extensions.
 */
@Data
public class MediaType {

    /**
     * The schema defining the type used for the request body.
     */
    private Schema schema;

    /**
     * Any	Example of the media type. The example object SHOULD be in the correct format as specified by the media type.
     * The example object is mutually exclusive of the examples object. Furthermore, if referencing a schema which contains an example,
     * the example value SHALL override the example provided by the schema.
     */
    private String example;

    /**
     * Examples of the media type. Each example object SHOULD match the media type and specified schema if present.
     * The examples object is mutually exclusive of the example object. Furthermore, if referencing a schema which contains an example,
     * the examples value SHALL override the example provided by the schema.
     */
    private Map<String, Example> examples;

    /**
     * A map between a property name and its encoding information. The key, being the property name, MUST exist in the schema as a property.
     * The encoding object SHALL only apply to requestBody objects when the media type is multipart or application/x-www-form-urlencoded.
     */
    private Map<String, Encoding> encoding;

}
