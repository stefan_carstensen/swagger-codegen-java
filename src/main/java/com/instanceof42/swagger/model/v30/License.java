package com.instanceof42.swagger.model.v30;

import lombok.Data;

/**
 * License information for the exposed API.
 * <p>
 * TODO This object MAY be extended with Specification Extensions.
 */
@Data
public class License {

    /**
     * REQUIRED. The license name used for the API.
     */
    private String name;

    /**
     * A URL to the license used for the API. MUST be in the format of a URL.
     */
    private String url;

}
