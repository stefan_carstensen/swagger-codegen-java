package com.instanceof42.swagger.model.v30;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * An object representing a Server Variable for server URL template substitution.
 * TODO This object MAY be extended with Specification Extensions.
 */
@ToString
@EqualsAndHashCode
public class ServerVariable {

    /**
     * An enumeration of string values to be used if the substitution options are from a limited set.
     */
    private List<String> _enum;

    /**
     * REQUIRED. The default value to use for substitution, and to send, if an alternate value is not supplied.
     * Unlike the Schema Object's default, this value MUST be provided by the consumer.
     */
    private String _default;

    /**
     * An optional description for the server variable. CommonMark syntax MAY be used for rich text representation.
     */
    @Getter
    @Setter
    private String description;


    public List<String> getEnum() {
        return _enum;
    }

    public void setEnum(List<String> _enum) {
        this._enum = _enum;
    }

    public String getDefault() {
        return _default;
    }

    public void setDefault(String _default) {
        this._default = _default;
    }

}
