package com.instanceof42.swagger.model.v30;


import lombok.Data;

/**
 * The object provides metadata about the API. The metadata MAY be used by the clients if needed,
 * and MAY be presented in editing or documentation generation tools for convenience.
 */
@Data
public class Info {

    /**
     * REQUIRED. The title of the application.
     */
    private String title;

    /**
     * A short description of the application. CommonMark syntax MAY be used for rich text representation.
     */
    private String description;

    /**
     * A URL to the Terms of Service for the API. MUST be in the format of a URL.
     */
    private String termsOfService;

    /**
     * The contact information for the exposed API.
     */
    private Contact contact;

    /**
     * The license information for the exposed API.
     */
    private License license;

    /**
     * REQUIRED. The version of the OpenAPI document (which is distinct from the OpenAPI Specification version or the API implementation version).
     */
    private String version;


}
