package com.instanceof42.swagger.reader;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.instanceof42.swagger.model.v30.Document;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class ReaderTest {

    private static final Logger LOG = LogManager.getLogger(ReaderTest.class);


    @Test
    public void readYaml30() throws IOException {

        File resourcesDirectory = new File("src/test/resource");
        LOG.info(resourcesDirectory.getAbsoluteFile());

        File file = new File(resourcesDirectory.getAbsoluteFile(), "examples/v3.0/petstore-expanded.yaml");

        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        Document document = mapper.readValue(file, Document.class);
        LOG.info(ReflectionToStringBuilder.toString(document, ToStringStyle.MULTI_LINE_STYLE));

    }


}
